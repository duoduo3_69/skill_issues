android的相关问题
=====

###
结构
    * android 开发环境搭建相关
	* misc

##android 开发环境搭建相关
### 具体内容和步骤自己去网上找吧 主要就是环境变量和各种 next
jdk
---
    http://www.oracle.com/technetwork/java/javase/downloads/index.html

sdk
---
    Android SDK: http://developer.android.com/sdk/installing/index.html
	sdk-tool only: http://developer.android.com/sdk/index.html#Other

ndk (optional)
---
    http://developer.android.com/tools/sdk/ndk/index.html


developing tool set
---
	android studio(recommend): http://developer.android.com/sdk/installing/index.html?pkg=studio
	eclipse: http://developer.android.com/tools/help/adt.html
	


## 常用软件库 library

okhttp
----
    compile 'com.squareup.okhttp:okhttp-urlconnection:2.2.0'
    compile 'com.squareup.okhttp:okhttp:2.2.0'
	
	```java
	package com.hianthy.derek.places.networkUtil;

	/**
	 * Created by derek on 24/01/15.
	 */
	import com.android.volley.toolbox.HurlStack;
	import com.squareup.okhttp.OkHttpClient;
	import com.squareup.okhttp.OkUrlFactory;

	import java.io.IOException;
	import java.net.HttpURLConnection;
	import java.net.URL;

	/**
	 * An {@link com.android.volley.toolbox.HttpStack HttpStack} implementation which
	 * uses OkHttp as its transport.
	 */
	public class OkHttpStack extends HurlStack {
		private final OkUrlFactory mFactory;

		public OkHttpStack() {
			this(new OkHttpClient());
		}

		public OkHttpStack(OkHttpClient client) {
			if (client == null) {
				throw new NullPointerException("Client must not be null.");
			}
			mFactory = new OkUrlFactory(client);
		}

		@Override protected HttpURLConnection createConnection(URL url) throws IOException {
			return mFactory.open(url);
		}
	}
	```
	```java
			//was this
	        RequestQueue queue = Volley.newRequestQueue(this);
        
	        ///now this
	        Network network = new BasicNetwork(new OkHttpStack());
	        RequestQueue queue = new RequestQueue(new DiskBasedCache(new File(getCacheDir(), "volley")), network);
	        queue.start();
	```
	[https://gist.github.com/JakeWharton/5616899]
	[https://gist.github.com/Jaymo/e13c939516f73d18a95e]
	[https://gist.github.com/ceram1/8254f7a68d81172c1669]

volley
---
	import from either
		https://android.googlesource.com/platform/frameworks/volley
			or
		https://github.com/mcxiaoke/android-volley
			or 
			
				```script
				for Maven
				format: jar

				<dependency>
				    <groupId>com.mcxiaoke.volley</groupId>
				    <artifactId>library</artifactId>
				    <version>{latest-version}</version>
				</dependency>
				for Gradle

				format: jar

				compile 'com.mcxiaoke.volley:library:1.0.+'
				format: aar

				compile 'com.mcxiaoke.volley:library:1.0.+@aar'
				```

retrofit
---
    compile 'com.squareup.retrofit:retrofit:1.9.0'

